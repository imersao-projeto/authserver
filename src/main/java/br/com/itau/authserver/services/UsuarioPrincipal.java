package br.com.itau.authserver.services;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import br.com.itau.authserver.models.Usuario;

public class UsuarioPrincipal implements UserDetails {
	private static final long serialVersionUID = 6635678591604984644L;
	private Usuario usuario;
	
	public UsuarioPrincipal(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return AuthorityUtils.createAuthorityList("ADMIN");
	}

	@Override
	public String getPassword() {
		return this.usuario.getSenha();
	}

	@Override
	public String getUsername() {
		return this.usuario.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return this.usuario.isAtivo();
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.usuario.isAtivo();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return this.usuario.isAtivo();
	}

	@Override
	public boolean isEnabled() {
		return this.usuario.isAtivo();
	}
}
