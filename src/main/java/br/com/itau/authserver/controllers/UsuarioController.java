package br.com.itau.authserver.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import br.com.itau.authserver.models.Usuario;
import br.com.itau.authserver.repositories.UsuarioRepository;

import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @RequestMapping(value="/usuario", method=RequestMethod.GET)
    public Iterable<Usuario> listUser(){
        return usuarioRepository.findAll();
    }

    @RequestMapping(value = "/usuario", method = RequestMethod.POST)
    public Usuario create(@RequestBody Usuario user){
        return usuarioRepository.save(user);
    }

    @RequestMapping(value = "/usuario/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable(value = "id") Long id) {
        usuarioRepository.deleteById(id);
        return "success";
    }

}
