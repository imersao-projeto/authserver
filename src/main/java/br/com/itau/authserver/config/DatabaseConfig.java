package br.com.itau.authserver.config;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.itau.authserver.models.Usuario;
import br.com.itau.authserver.repositories.UsuarioRepository;

@Configuration
public class DatabaseConfig {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	
	@Bean
	CommandLineRunner popularDadosIniciais() {
		return args -> {
			String password = encoder().encode("1234");
			if (!usuarioRepository.findByUsername("joao").isPresent()) {
				HashMap<String, String> atributosJoao = new HashMap<>();
				atributosJoao.put("cargo", "Coordenador");
				Usuario joao = new Usuario("joao", password, atributosJoao);
				usuarioRepository.save(joao);
			}
			if (!usuarioRepository.findByUsername("jose").isPresent()) {
				HashMap<String, String> atributosJose = new HashMap<>();
				atributosJose.put("cargo", "Funcionario");
				Usuario jose = new Usuario("jose", password, atributosJose);
				usuarioRepository.save(jose);
			}
			if (!usuarioRepository.findByUsername("maria").isPresent()) {
				HashMap<String, String> atributosMaria = new HashMap<>();
				atributosMaria.put("maria", "Funcionario");
				Usuario maria = new Usuario("maria", password, atributosMaria);
				usuarioRepository.save(maria);
			}
		};
	}
	
	@Bean
    public BCryptPasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }
	
}
