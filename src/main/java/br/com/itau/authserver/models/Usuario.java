package br.com.itau.authserver.models;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@NodeEntity
public class Usuario {
	@Id @GeneratedValue
	private Long id;
	
	private String username;
	
	private String senha;
	
	private boolean isAtivo;
		
	private String atributosUsuario;
	
	
	public Usuario() { }
	
	public Usuario(String username, String senha) {
		this(username, senha, new HashMap<String, String>(), true);
	}
	
	public Usuario(String username, String senha, Map<String, String> atributos) {
		this(username, senha, atributos, true);
	}
		
	public Usuario(String username, String senha, Map<String, String> atributos, boolean isAtivo) {
		setUsername(username);
		setSenha(senha);
		setAtributos(atributos);
		setAtivo(isAtivo);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String nome) {
		this.username = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public boolean isAtivo() {
		return isAtivo;
	}

	public void setAtivo(boolean isAtivo) {
		this.isAtivo = isAtivo;
	}

	protected String getAtributosSerializados() {
		return atributosUsuario;
	}

	protected void setAtributosSerializados(String atributosSerializados) {
		this.atributosUsuario = atributosSerializados;
	}
	
	public Map<String, String> getAtributos() {
		TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {};
		ObjectMapper mapper = new ObjectMapper();
		try {
			Map<String, String> map = mapper.readValue(getAtributosSerializados(), typeRef);
			return map;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public void setAtributos(Map<String, String> atributos) {
		ObjectMapper mapper = new ObjectMapper();
		String json;
		try {
			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(atributos);
			setAtributosSerializados(json);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			setAtributosSerializados("");
		}		
	}


}
