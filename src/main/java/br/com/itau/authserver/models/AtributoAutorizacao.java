package br.com.itau.authserver.models;

public class AtributoAutorizacao {
	public String chave;
	public String valor;
	
	public AtributoAutorizacao(String chave, String valor) {
		setChave(chave);
		setValor(valor);
	}
	
	
	public String getChave() {
		return chave;
	}
	public void setChave(String chave) {
		this.chave = chave;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
}
