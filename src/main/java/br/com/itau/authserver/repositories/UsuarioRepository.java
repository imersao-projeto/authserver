package br.com.itau.authserver.repositories;

import java.util.Optional;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import br.com.itau.authserver.models.Usuario;

public interface UsuarioRepository extends Neo4jRepository<Usuario, Long> {
	Optional<Usuario> findByUsername(String username);
}
